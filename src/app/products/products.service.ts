import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {

productsObservable;
// getProducts(){
// this.productsObservable=this.af.database.list('/products');
// return this.productsObservable;
// }

//get users from firebase with join to users
getProducts(){
this.productsObservable=this.af.database.list('/product').map(
products => 
  {products.map( product=> {
    product.categotyName=[];
    product.categotyName.push(this.af.database.object('/category/' + product.categoryId));

    });
    return products;
  }
);
return this.productsObservable;
}
deleteProduct(product){
let postKey=product.$key;
this.af.database.object('/product/'+postKey).remove();
 }

  constructor(private af:AngularFire) { }

}
