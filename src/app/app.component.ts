import { Component } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Component({

  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shai app works ?!';
//add in for angularfire
  constructor(af:AngularFire){
    console.log(af);
  }
}
