
import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

isLoading:Boolean = true;  // בודק אם קיבלתי ערכים, בדיפולט הוא טרו 
users;

currentUser;

select(user){
this.currentUser = user;  // li מגיע מ user 
}

  constructor(private _usersService:UsersService) { }

updateUser(user){

this._usersService.updateUser(user);

}



  deleteUser(user){

    this.users.splice(
       
         this.users.indexOf(user),1

    )
  }

 adduser(user){       // פונק המוסיפה ערך שמוכנס 

   this._usersService.addUser(user);


  }

  ngOnInit()  // פונקציה שמופעלת בזמן טעינת הקובץ, נרצה שהנתונים יטענו
  {
    this._usersService.getUsers().subscribe(usersData => {this.users = usersData;
    this.isLoading = false; // זה לאחר שהנתונים עלו אנו רוצים להפוך לפולס
    console.log(this.users)});  
  }

}
